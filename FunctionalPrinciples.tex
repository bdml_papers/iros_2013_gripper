\section{Functional Principles}
Grasping a surface dynamically requires several properties for the gripper, whether for perching MAVs on a surface in Earth's gravity or grappling a target in space. This section generalizes the problem of dynamic surface grasping and describes several functional principles that must be embodied by a gripper using directional adhesives.

\label{sec:functional_principles}

\begin{figure}
	\centering
	\includegraphics[width=\columnwidth]{./Images/Functional.pdf}
	\caption{Illustration of the Functional Principles described in Sec.~\ref{sec:functional_principles}.}
	\label{fig:functional}
\end{figure}

\subsection{Dynamic Passive Alignment}

	When the grasper first makes contact with the surface, it is unlikely that the adhesive tiles will be aligned. Hence the grasper must compensate for misalignment before or during the collision (Fig.~\ref{fig:functional},~\emph{A}).  A passive alignment system can be lighter, simpler, and  more robust than an actuated system.

	For a passive system, it is important that the work required for alignment is small compared to the grasper's kinetic energy in order to prevent rebounding before alignment has occurred. The system should therefore have low moments of inertia and rotational stiffnesses.

\subsection{Rebound Mitigation}

	The remaining kinetic energy of the grasper must be absorbed during the collision or during rebound (Fig.~\ref{fig:functional},~\emph{B}). The maximum energy that can be absorbed is limited by the size of the device and the energy absorbing force. The energy absorbing force is itself limited. During collision, it must not damage the device; and during rebound, it must not exceed the adhesion limits of the adhesive tiles.

\subsection{Adhesive Loading}

	Unlike pressure sensitive adhesives, directional adhesives are not sensitive to normal preload \cite{SantosJAST}: simply pressing them into the surface will not make them stick. Directional adhesives produce negligible adhesion unless shear force is applied in the correct direction to turn the adhesive ``ON'' (Fig.~\ref{fig:functional},~\emph{C}). In order to support normal loads without shear, the grasper must use multiple tiles of directional adhesive which are loaded with internal shear forces in opposing directions.

	With an appropriate mechanism, the energy of the collision can be exploited to passively create these forces and turn the adhesives ``ON'' at the appropriate time. Excessive shear force will cause the directional adhesives to fail, so the mechanism must ensure the shear force lies within acceptable limits. The excess energy must be dissipated or stored elsewhere. Alternatively, the forces may be produced by an active mechanism. All adhesive tiles must be aligned and in contact with the surface before the adhesives are loaded, so an active mechanism must have accurate sensing to ensure correct timing.

\subsection{System Locking}

	Once the internal shear force has been applied to the adhesives and as much energy as possible has been absorbed during the collision, the grasper must enter a locked state to keep the internal shear forces in place and store the absorbed energy. (Fig.~\ref{fig:functional},~\emph{D}). This can be achieved passively using a ratchet or latch.

\subsection{Resistance to Arbitrary Wrenches}

	The grasper must be able to support arbitrary wrenches, i.e. combinations of applied forces and moments  (Fig.~\ref{fig:functional},~\emph{E}). Ideally, the grasper mechanism should distribute these loads optimally to limit the maximum force on the adhesive, so that the grasper's force limit equals the combined force limits of the separate individual adhesive tiles. 

	This is not straightforward because the tiles are initially misaligned on the surface, and their positions change during the collision. Therefore, the grasper mechanism must compensate by taking up any slack in the loading tendons, and it must distribute loads optimally despite this compensation.

\subsection{Releasing the Grasp}

	For directional adhesives, it is not necessary to apply a detachment force. When releasing the grasp is desired, a release mechanism can disengage the system lock to release the internal shear loads and turn the adhesives ``OFF.''  This allows the stored energy, if any, to push the surface and grasper apart (Fig.~\ref{fig:functional},~\emph{F}).