\section{Introduction}
\label{sec:introduction}

	The ability to grasp flat or gently curved surfaces repeatably and releasably has several compelling robotic applications including the perching of micro air vehicles (MAVs) on walls or ceilings (Fig.~\ref{fig:perch}) and the grappling of orbital debris in space. In both applications, low attachment and detachment forces are required; however an important difference is that MAVs are low mass and high velocity whereas orbital debris typically has a larger mass and lower relative velocity.
%mrc got rid of "would"

	Directional, gecko-inspired adhesives are suitable for these applications because they require little energy for attachment and detachment, work on many surfaces, can undergo many attach/release cycles \cite{ParnessRSI}, and can be scaled to either small or large applications \cite{Hawkes2013}. 
%mrc 'smaller' or 'larger' (than what?) --> 'small', 'large'
	Because the adhesives rely only on van der Waals forces to stick, they are compatible with spaceflight applications where nearly all pressure-sensitive adhesives are prohibited because of radiation, temperature and outgassing issues. Further, many of these applications require the ability to attach and release with low force, making gecko-inspired adhesives particularly appropriate.
%mrc 'more desirable' (than what?) --> rewrote sentence a bit.

\begin{figure}
	\centering
	\includegraphics[width=\columnwidth]{./Images/QuadrotorFrontPage.jpg}
	\caption{Quadrotor Micro Air Vehicle hanging from a glass surface using the directional adhesive Collapsing Truss Grasper 
		(Sec.~\ref{sec:collapsing_truss_grasper}).}
	\label{fig:perch}
\end{figure}

The work presented here builds upon prior work on climbing robots, perching MAVs, and gecko-inspired adhesives. Unlike a robot climbing a wall, which can control the position, orientation and contact forces of its feet (e.g.~\cite{Kim2008,Murphy_2010}), the applications considered here involve either a grasper or a target that is in free flight. The entire collision event typically lasts less than 0.1\,s between initial contact and equilibrium (Fig.~\ref{fig:landing}). 
%mrc Moved bit below to here because it seemed to flow better.
As in other recent work \cite{Hawkes2013}, the devices use directional adhesives mounted to arrays of rigid tiles, loaded with central tendons. This scheme ensures that the adhesive area is loaded evenly and no moments are transferred from the device, which would cause stress concentrations and premature failure.

\begin{figure}
	\centering
	\includegraphics[width=\columnwidth]{./Images/GripperLandingFigure.pdf}
	\caption{A) MAV approaches surface with misalignment.  B) Landing mechanism passively aligns to surface.  C) Truss structure 
		collapses to absorb energy; tendons tension to load adhesive tiles; system locks in place.  D) Slight rebound stretches 
		preloaded spring, which prevents overloading adhesives.}
	\label{fig:landing}
\end{figure}

	Previous work on MAVs that can perch on walls and other flat surfaces has exploited spines or arrays of spines 
\cite{Kovac:2010, Lussier-DesbiensIJRR11}, sticky materials \cite{anderson2009sticky}, and dry adhesives \cite{daler2013perching}. The present work is aimed at small rotorcraft that can fly at several meters per second and takes advantage of directional adhesives capable of sticking and releasing rapidly and with very low effort \cite{ParnessRSI,easonmicro}. Work on the control of MAVs in confined spaces (e.g.~\cite{Michael2010, mellinger2012trajectory, Humbert2010, ICRA12perchingROA}) is also relevant for establishing the range of velocities and orientations that may be expected at contact.

	The problem of space debris is increasingly of concern to space agencies around the world.  There are currently over 1500 rocket bodies and over 10,000 other debris objects in Earth orbit \cite{JSCOD}. In 2007, a piece of debris collided with an active communication satellite causing a total loss worth many millions of dollars in damage. With the mechanisms presented here, ``non-cooperative'' targets can be acquired, in contrast to previous systems which have relied on pre-installed grapple features on cooperative targets. For example the arms on the Space Shuttle and the International Space Station. Similarly, the Orbital Express mission demonstrated docking with a cooperative target \cite{OrbitalExpress,OrbitalExpress2}. The FREND arm is planned for use aboard the DARPA Phoenix mission and is expected to grasp a non-cooperative target using a Marman Clamp, a fixed hard point on the side of the spacecraft \cite{FREND}. 
The devices presented here do not require specialized fixtures and can attach to flat or gently curved smooth surfaces including solar panels and the sides of spacecraft, fuel tanks, etc. They have the potential to simplify orbital debris clearance, making it more robust and less reliant on precision sensing and navigation. 

	The following section of this paper presents a set of functional principles for grasping surfaces under dynamic conditions, when either the grasper or target is in free flight. Next, prototype designs embodying these principles are described. Modeling and testing results show that these designs are capable of absorbing collision energy and using it to align the surfaces and apply loads to the directional adhesives, causing them to attach without bouncing away. The paper concludes with a discussion of ongoing work to incorporate these prototypes into MAVs and into space grappling devices for environment testing to simulate orbital conditions \cite{GeckoBigSky}.
