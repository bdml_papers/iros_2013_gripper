\section{Results}

\subsection{Collapsing Truss Grasper: Limit Curves}
\label{sec:collapsing_truss_limit_curve}

\begin{figure}
	\centering
	\includegraphics[width=\columnwidth]{./Images/measuredlimitcurves.pdf}
	\caption{A) Measured limit curve of a single 1$\times$1 cm adhesive tile used in the collapsing truss grasper. Adhesive forces 	
		are plotted as negative values. B) Measured limit curve of the collapsing truss grasper,	which uses two 1$\times$1 cm 
		adhesive tiles.}
	\label{fig:limit_curve_tile}
\end{figure}

	The limit curve of one of the adhesive tiles used in the Collapsing Truss Grasper was measured using a motorized positioning stage and a six-axis force-torque transducer, using methods described in \cite{easonmicro}. This limit curve is plotted in Fig.~\ref{fig:limit_curve_tile},~A. Note that the limit curve goes through the origin, indicating that no adhesion is produced without shear force.

	By drawing a line segment in Fig.~\ref{fig:limit_curve_tile},~A at the angle of the tendons (measured to be $\theta$ = 13 deg), approximate values of $S_{max} = 4$~N and $N_{max} = 1$~N can be determined. Based on the simple model of Fig.~\ref{fig:adhesive_loading_model}, it is predicted that the grasper's combined limit curve will have a maximum normal force of $2N_{max} = 2$~N and a maximum shear force of $S_{max} = 4$~N (or more). Next, the combined limit curve of the grasper was also measured; the resulting data are shown in Fig.~\ref{fig:limit_curve_tile},~B. While there is some amount of asymmetry due to variations between the two adhesive tiles, the maximum normal and shear forces agree well with the predictions from the simple model.

\subsection{Collapsing Truss Grasper: Preload}
\label{sec:collapsing_truss_preload}

	The maximum adhesion of the Collapsing Truss Grasper with varying amounts of normal preload was measured by pressing the grasper into a glass surface using weights of various sizes, and then pulling the grasper perpendicularly off the surface and recording the maximum normal force.

	The Truss Spring was removed for this test to decrease the force required to collapse the truss. This removes the capability of the device to absorb energy during collision, but this would be acceptable in a low-energy application. The test data, plotted in Fig.~\ref{fig:preload}, indicate that the normal preload has no observable effect on the maximum adhesive load: the grasper can be used with any preload in the range of 0.3 to 2.4~N with an essentially constant normal adhesion (2 N). However, a preload smaller than 0.3~N is not sufficient to engage the mechanism's latch. The adhesive tiles themselves appear to be very insensitive to preload, as has been previously shown \cite{SantosJAST}.

\begin{figure}
	\centering
	\includegraphics[width=.85\columnwidth]{./Images/AdhesionVPreload.pdf}
	\caption{Graph showing the amount of adhesion force generated versus normal preload force applied for the collapsing truss 
		grasper.  The adhesion force is independent of preload beyond some very small threshold. }
	\label{fig:preload}
\end{figure}

\subsection{MAV graspers: Energy absorption}
\label{sec:pivot_linkage_energy_absorption}

	An experiment was conducted to investigate energy absorption during collision. The MAV Pivot Linkage Grasper was tested by dropping it onto a surface and measuring the rebound height (the adhesives were removed for this test). This experiment was repeated with different incoming kinetic energies and with three different Energy Absorbing configurations: linear springs with a ratchet, nonlinear springs (near constant-force) with a ratchet, and no springs. These data are plotted in Fig.~\ref{fig:rebound_energy}, left.

	For nearly all incoming kinetic energies tested, the rebound energies of the three configurations were in a specific order, with the no-spring configuration being highest and the nonlinear spring being lowest. This indicates that the nonlinear spring absorbs the most energy during collision of all the energy absorbing configurations.

	To compare this energy with the energy absorbed during rebound, a force vs. displacement curve was measured for the Rebound Spring used by the MAV graspers (Fig.~\ref{fig:rebound_energy}, right). Given the maximum normal load the grasper can support (e.g. 2~N for the Collapsing Truss Grasper), the maximum permissible rebound energy is the integral of the curve up to the maximum load (red region in Fig.~\ref{fig:rebound_energy}), which is approximately 29~mJ. This energy is plotted as a red dashed line in Fig.~\ref{fig:rebound_energy}, left. This plot can be used to approximate the maximum permissible incoming kinetic energy for the three damping configurations, with the result that the nonlinear spring allows nearly twice as much kinetic energy as the linear spring and 4 times as much as no spring.

\begin{figure}
	\centering
	\includegraphics[width=\columnwidth]{./Images/updated_rebound_height2.pdf}
	\caption{ Left: Rebound energy vs. collision energy for three different energy absorbing 
		configurations. Right: Force vs. displacement of the Rebound Spring.}
	\label{fig:rebound_energy}
\end{figure}

\subsection{Pivot Linkage Grasper: Scalability and Dynamic Capture}

	The scalability of grasper designs was investigated by testing the Space Pivot Linkage Grasper at higher loads. Pull tests were performed on a variety of surfaces using a digital force gauge. Successful grasping was demonstrated on a variety of surfaces including satellite solar panels. A maximum normal adhesive force of $>$60~N was demonstrated. Tests were also performed to verify the load-sharing function of the tendons with carefully selected tiles. The normal adhesion of the grasper was found to be four times as large as the lowest-performing tile. When a set of opposing tiles were intentionally separated from the surface, hard stops in the mechanism allowed the remaining two tiles to demonstrate a contact strength of twice the lowest-performing tile.

	Dynamic capture tests were performed with a human holding the grasper system in place of  the FREND robotic arm that will be used in future testing \cite{FREND}. Using a simulated piece of debris (a 33~kg foot locker with wheels and several space-like surfaces mounted), capture experiments were performed at a variety of relative velocities and spin rates. The maximum successful capture had relative motion of over 2~m/s and a spin rate of $>$75~deg/s. This demonstration is included in the paper's accompanying video.