\section{Design}
\label{sec:design}
Two designs are presented that display the functional principles of dynamic surface grasping. The first, a collapsing truss design, is sized for use on a MAV. The second, a pivoting linkage design, has been sized and fabricated both for use on a MAV and as a prototype for future use in Earth orbit to grapple orbital debris.

\subsection{Collapsing Truss Grasper}
\label{sec:collapsing_truss_grasper}

	This grasper design is based on a collapsing truss mechanism (Fig.~\ref{fig:collapsing_truss_grasper}). It is designed as low-mass landing gear (3.5~g) for a 120~g MAV, and uses 2 adhesive tiles (1$\times$1~cm square). To decrease the pitch-back moment when the MAV is attached to a wall, the Collapsing Truss Grasper is designed to be low profile in the collapsed position. The grasper is designed in accordance with the functional principles presented in Sec.~\ref{sec:functional_principles}.

	The truss is attached to the MAV at its apex by a single tendon which passes through a compliant foam joint, which keeps the grasper aligned to the MAV during flight but allows it to rotate and translate during a collision. Translation is necessary because one tile of adhesive makes contact before the other, and the tiles resist sliding. The grasper uses a set of outriggers to decrease the alignment force and ensure it is partially aligned before contact (\textbf{Dynamic Passive Alignment}). A model of this alignment system is described in Sec.~\ref{sec:modeling_alignment}.

	As the truss collapses, the Truss Tendon routed between the two legs of the truss becomes taut. This pulls the center of the Tile Tendon against the bottom of the truss, applying shear forces to the adhesive tiles and turning them ``ON.'' The internal shear force is limited by the length of the Truss Tendon (\textbf{Adhesive Loading}). Energy is absorbed during the collision by the Truss Spring. When the truss collapses fully, a latch engages to lock the truss in the collapsed state (\textbf{System Locking}). If desired, the Truss Spring can be removed to minimize the amount of normal force required to collapse the truss. Extra energy is absorbed by the Rebound Spring, which is attached to the tendon through the compliant foam joint (\textbf{Rebound Mitigation}). This spring is preloaded in order to keep the truss pulled tight to the MAV and because a preloaded spring can absorb more energy in this situation  (see Sec.~\ref{sec:pivot_linkage_energy_absorption}).

	Once the grasper is locked in place, the Tile Tendon remains under tension and stays at an essentially constant angle, geometrically defined by the length of the Tile Tendon and the distance between the tiles. When a large external load is applied (e.g. wind on the MAV), this load is distributed between the two tiles and additional tension is applied to the Tile Tendon, adding more internal shear force, which produces more adhesion due to the directional nature of the adhesives (\textbf{Resistance to Arbitrary Wrenches}). The Tile Tendon angle can be fine-tuned to change the performance characteristics of the grasper, as described in Sec.~\ref{sec:modeling_adhesive_loading}.

\begin{figure}
	\centering
	\includegraphics[width=\columnwidth]{./Images/collapsingtruss_ericlabels.pdf}
	\caption{Collapsing Truss Grasper. A) Schematic showing functional components.  B) Device in locked state (grasping a 
		surface).}
	\label{fig:collapsing_truss_grasper}
\end{figure}

\subsection{Pivot Linkage Grasper}
\label{sec:pivot_linkage_grasper}

	The other grasper design uses a pivoting linkage to apply tension to the Tile Tendons. Unlike the Collapsing Truss Grasper, the adhesive tiles are loaded with semi-independent mechanisms, so the Pivot Linkage Grasper can have a larger number of adhesive tiles. Two versions of this design are presented, each using 4 adhesive tiles: The MAV Pivot Linkage Grasper is designed as landing gear for a 120~g MAV and uses 1$\times$1~cm square adhesive tiles (Fig.~\ref{fig:MAV_pivot_linkage_grasper}); and the Space Pivot Linkage Grasper is designed as a prototype for grappling operations in Earth orbit and uses 4$\times$4~cm square adhesive tiles (Fig.~\ref{fig:space_pivot_linkage_grasper}).

	The mechanisms are actuated by pressing the Center Plate and the Baseplate together. This causes the Tensioning Arms to rotate around the Pivots and apply force to the Tile Tendons through the Tendon Springs. The MAV version uses tendons that pull inwards, crossing under the center of the Baseplate for compactness, while the Space version uses tendons that pull outward to enable grasping flexible surfaces such as thermal blankets.

	The MAV Pivot Linkage Grasper uses the energy of collision to turn ``ON'' the adhesive tiles. It requires a larger normal preload force than the Collapsing Truss Grasper to apply the internal shear forces to the adhesive tiles. This is partly because it has less mechanical advantage, but also because the system of 4 tiles is over-constrained and therefore some amount of preload is necessary to deflect the Tile Support Foam and bring all tiles into contact. Once the tiles make contact, the Tendon Springs compensate for any initial misalignment of the adhesive tiles. In the MAV grasper, the Tendon Springs are preloaded and nonlinear, producing a nearly constant force over a large range of deflection to ensure that all 4 tiles are loaded evenly throughout the collision.

\begin{figure}
	\centering
	\includegraphics[width=\columnwidth]{./Images/pivotlinkage_labels.pdf}
	\caption{MAV Pivot Linkage Grasper. A) Schematic showing functional components.  B) Device in locked state (grasping a 
		surface).}
	\label{fig:MAV_pivot_linkage_grasper}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=\columnwidth]{./Images/JPLSpacePivotLinkage2.pdf}
	\caption{Space Pivot Linkage Grasper shown in the locked state (grasping a surface).}
	\label{fig:space_pivot_linkage_grasper}
\end{figure}

	The Space Pivot Linkage Grasper works similarly but can also function in absence of a collision by turning the Leadscrew. This actively applies the shear load to the adhesive tiles without requiring a normal force, so preload is only required to deflect the Tile Support Foam. In the Space grasper, the Tendon Springs are linear, but the Leadscrew allows the grasper to control the tension as necessary: for example, a lower tension could be used when grasping a rougher surface to prevent the adhesives from failing prematurely, but a higher tension could be used on a smoother surface to increase the grasper's loadbearing capacity.

	Kinetic energy is absorbed by the Energy Absorbers and locked in place using ratchets or a Ratcheting Nut. These ratchet systems may lock at multiple points, which allows the Pivot Linkage Graspers to absorb a variable amount of energy during different collisions (unlike the Collapsing Truss Grasper). In addition, the Energy Absorbers have nonlinear stiffness to provide maximum deceleration in a short distance (Sec.~\ref{sec:pivot_linkage_energy_absorption}). A rebound spring may be added to the MAV grasper to absorb additional energy; alternatively, the Space grasper is intended to be mounted on a compliant robotic arm which may be used for active rebound mitigation. 

	After a Pivot Linkage Grasper is locked and external loads are applied, the Tile Tendons behave as if they were inextensible. This is because the Tendon Springs do not stretch or relax until the external load is high enough to balance the external work with the energy change in the springs. Since the Tile Tendons do not change length or tension, the applied loads are instead reacted by changes in Tile Tendon angle and corresponding deflections of the Tile Support Foam.
