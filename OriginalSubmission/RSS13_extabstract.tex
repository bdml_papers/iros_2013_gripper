% \begin{document}
\documentclass[letterpaper, 10 pt, final]{IEEEconf}  % Comment this line out
\usepackage{amsmath}                                                          % if you need a4paper
%\documentclass[a4paper, 10pt, conference]{ieeeconf}      % Use this line for a4
                              % paper
% \IEEEoverridecommandlockouts                              % This command is only
                              % needed if you want to
                              % use the \thanks command
% \overrideIEEEmargins
% See the \addtolength command later in the file to balance the column lengths
% on the last page of the document
% The following packages can be found on http:\\www.ctan.org
\usepackage{graphicx} % for pdf, bitmapped graphics files
%\usepackage{epsfig} % for postscript graphics files
%\usepackage{mathptmx} % assumes new font selection scheme installed
%\usepackage{times} % assumes new font selection scheme installed

\renewcommand{\vec}[1]{\mathbf{#1}}

\usepackage{color}
\newcommand{\todo}[1]{\textcolor{blue}{#1}}

\title{\LARGE \bf Dynamic Surface Grasping for UAVs}
\author{E.W.~Hawkes$^1$, E.V.~Eason$^2$, H. Jiang$^1$, M.A.~Estrada$^1$, M.T.~Pope$^1$, D.L.~Christensen$^1$\\ 
M.R.~Cutkosky$^1$, A. Kehlenbeck$^3$, J.S. Humbert$^3$% <-this % stops a space
\thanks{1. Mechanical Engineering Dept., 2. Applied Physics Dept., Stanford University, Stanford, CA, USA}
\thanks{3. Dept. of Aerospace Engineering, University of Maryland, MD, USA}
}


\begin{document}
\maketitle
\thispagestyle{empty}
\pagestyle{empty}
                              
                              
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}
By employing directional adhesion, small UAVs can attach to walls and ceilings 
with a dynamic maneuver that absorbs kinetic energy on contact and uses it
to load adhesive pads in opposition. To detach, it
suffices to release the stored energy. Models of the landing process reveal design
choices that satisfy constraints on the normal and tangential 
contact forces, so that the adhesives are loaded smoothly and without failures
that could damage the mechanism or lead to slipping
or bouncing off the surface. Ongoing work focuses on integrating the 
modeling and design of mechanisms with sensing and control during the final moments of flight
to increase the robustness of the maneuver and allow for failure recovery.
\end{abstract}
                              
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction}
\label{intro}

The ability of small UAVs to land on walls and ceilings promises to greatly expand their capabilities for applications ranging from 
monitoring air quality and inspecting large structures to search and rescue. While landed, they consume little power, greatly extending mission life, and provide a stable platform for sensing, communication, etc. 
The work described here builds upon prior work on climbing robots and the control of small rotorcraft to enable UAVs to land and perch on walls and ceilings. For rough surfaces such as concrete and brick, the grippers employ arrays of microspines inspired by the legs of insects 
\cite{Lussier-DesbiensIJRR11}. For smooth surfaces such as glass and metal panels, they employ gecko-inspired dry adhesives \cite{Kim2008}.

The advantages of perching on surfaces have been noted, with recent developments including UAVs that grip surfaces with velcro or small grippers \cite{Mellinger_grasp2013}, spines \cite{Kovac:2010,Lussier-DesbiensIJRR11} or microstructured polymers that are peeled to detach \cite{daler2013perching}. 

The solutions used in the present work rely on directional adhesion. The devices are not sticky in the default state, and therefore tend not to attract dirt. With the application of a controlled tangential force, they can sustain negative normal forces. 
To land on ceilings and inverted surfaces it is necessary to use opposed sets of adhesives, with an internal force, $f_t$, applied parallel to the surface (figure \ref{QuadrotorPerched}). Relaxing this force releases the UAV. With attention to the timing of applying and relaxing internal forces, attachment and detachment can be very smooth and low-effort, resulting in long lifetimes with tens of thousands of cycles possible without large reductions in performance \cite{ParnessRSI}. 

\begin{figure}
\centering
\includegraphics[width=0.95\columnwidth]{./Images/QuadrotorPerched.pdf}
\caption{An internal tangential force $f_t$ maintains adhesion at two opposed pads, from which a 95\,g UAV hangs after landing. }
\label{QuadrotorPerched}
\end{figure}

The sequence for landing on a surface, from first contact to equilibrium, takes less than 50\,ms (figure \ref{LandingSequence}). The dynamics are dominated by contact forces and there is little opportunity for aerodynamic control. This realization leads to the design of a passive mechanism that can absorb the kinetic energy, using some of it to load the adhesives for attachment, and dissipating or storing the remainder. 

\begin{figure}[b]
\centering
\includegraphics[width=0.9\columnwidth]{./Images/GripperLandingFigure.pdf}
\caption{Landing sequence involving initial alignment and compression of a damped truss mechanism that absorbs kinetic energy and loads adhesives for attachment. Initial velocity is 0.75-1.5\,m/s.
}
\label{LandingSequence}
\end{figure}

When considering the design of the landing and attachment mechanism, it is helpful to start with functional constraints imposed by the adhesives. As seen in figure \ref{SimpleLimits}, for pads of directional dry adhesives, the normal and tangential forces must remain within a composite limit surface to sustain attachment. Note that increasing the magnitude of the internal force, $f_t = f_l cos\theta$, increases the maximum downward normal force, $f_n = 2(f_l sin\theta-f_k)$ that can be supported. Relaxing the internal force causes detachment.  

The landing dynamics, involving linear and angular motions, and sliding where the outrigger makes contact, are complex. Modeling this hybrid discrete and continuous system in MotionGenesis \cite{MG} is an area of ongoing work. However, some insight can be obtained from the simplest case with a purely normal approach velocity. The initial kinetic energy, $1/2mv_0^2$, must be absorbed without exceeding maximum normal and tangential forces, $f_n$ and $f_t$ respectively, at the pads. To slow the UAV rapidly without exceeding these constraints, the mechanism should be a nonlinear damped spring. As shown in figure \ref{rebound} (right), a nonlinear rebound spring absorbs energy efficiently for the maximum rebound force of 2\,N that the adhesives can sustain. In drop tests (without adhesion) this maximum force corresponds to a rebound height of 20\,mm, denoted by a corresponding red dashed line in figure \ref{rebound} (left). Additionally, with a nonlinear main spring, this rebound is obtained with a higher initial drop height (equivalent to a higher contact velocity where $v_0 = \sqrt{2gh}$) than is possible with a linear spring.

The energy needed to load the tendons is considerably lower than $1/2mv_0^2$. To dissipate this energy with damping is also a challenge using lightweight foams; therefore it is captured and stored internally using a latch (figure \ref{TrussDevice}). To detach the UAV, the latch is released, resulting in a modest jumping force to assist takeoff.

\begin{figure}[ht]
\centering
\includegraphics[width=0.8\columnwidth]{./Images/SimpleLimits.pdf}
\caption{(a) schematic of two adhesive pads loaded by tendons ($f_l$) and foam springs ($f_k$), (b) idealized limit surface showing where various external force vectors, $\vec{f}_e$ lie, (c) empirical limit surface for two pads loaded with tendons.
}
\label{SimpleLimits}
\end{figure}

\begin{figure}[htb]
\centering
\includegraphics[width=0.9\columnwidth]{./Images/rebound_height.pdf}
\caption{(right) A nonlinear rebound spring absorbs energy rapidly for a maximum force of 2\,N. (left) This force corresponds to a maximum rebound height of 20\,mm, which dictates max drop height for different designs.}
\label{rebound}
\end{figure}

\begin{figure}[htb]
%MRC Use \centering
%\begin{center}
\centering
%MRC Fixed figure to get rid of typo (Absorbing)
\includegraphics[width=0.8\columnwidth]{./Images/grappling_device6.pdf}
%\includegraphics[width=\columnwidth]{./Images/LandingAbsorbingSpring.pdf}
%DLC Ahhh! I was updating the same figure at the same time!  this is why none of my opacity changes fixed the missing arrows! fixed same typo in the new master
\caption{(A) In unloaded state, outriggers provide initial alignment. As adhesive pads make contact, the mechanism collapses, storing energy in a spring and ultimately loading tendons to create internal force. An additional spring reduces the maximum rebound force. (B) A latch keeps the mechanism locked until ready for takeoff.}
\label{TrussDevice}
%\end{center}
\end{figure}

The mechanism and the surface properties determine an envelope of initial conditions that can result in attachment. Among these, the velocity normal to the surface and the pitch and roll rates are particularly important. The complementary problem for the UAV controller is to ensure that the vehicle is within this envelope prior to contact, despite disturbances. An area of particular interest is to use optical sensors that take advantage of the wall or ceiling proximity \cite{Humbert2010}.

\section{Acknowledgments}
This work is supported by NSF (IIS 1161679) and ARL MAST (MCE-13-4.4). Hawkes, Eason and Estrada are additionally supported by NSF graduate fellowships.

\bibliographystyle{IEEEtran}
\bibliography{citations}
\end{document}